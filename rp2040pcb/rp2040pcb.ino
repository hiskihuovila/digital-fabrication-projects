#include <Adafruit_NeoPixel.h>
#include <TM1637.h>

#define NUMPIXELS 1

int neoout = 26;
int neoin = 27;
int led = 28;
int button_pullup = 29;

int smallpixel_power = 11;
int smallpixel_in  = 12;
float hue = 0;
bool state = true;
bool wasopen = true;

Adafruit_NeoPixel bigpixel(NUMPIXELS, neoin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel smallpixel(NUMPIXELS, smallpixel_in, NEO_GRB + NEO_KHZ800);

const int CLK = 2;
const int DIO = 4;
TM1637 tm1637(CLK, DIO);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

  pinMode(button_pullup, INPUT_PULLUP);
  bigpixel.begin();
  bigpixel.setBrightness(10);
  bigpixel.clear();

  pinMode(smallpixel_power, OUTPUT);
  digitalWrite(smallpixel_power,HIGH);

  smallpixel.begin();
  smallpixel.setBrightness(10);
  smallpixel.clear();

  pinMode(PIN_LED_G,OUTPUT);
  pinMode(PIN_LED_R,OUTPUT);
  pinMode(PIN_LED_B,OUTPUT);
  
  digitalWrite(PIN_LED_R, LOW);
  digitalWrite(PIN_LED_G,HIGH);
  digitalWrite(PIN_LED_B,HIGH);

  pinMode(CLK,OUTPUT);
  pinMode(DIO,OUTPUT);

  tm1637.init();
  tm1637.set(BRIGHTEST);
  Digitdisplay();
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
      Serial.println("Set hue between 0 and 360, hue set to 0");
      hue = 0;
    }
    Digitdisplay();
  }
  
  //button is open
  if(digitalRead(button_pullup) == HIGH) {
      if(!wasopen) {
        wasopen = true;
      }
      if(state){
        bigpixel.clear();
        smallpixel.clear();
        float to16bit = (hue / 360.0) * 65535.0;
       
        bigpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        bigpixel.show();
       
        smallpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        smallpixel.show();

        digitalWrite(PIN_LED_R, HIGH);
        digitalWrite(PIN_LED_G,HIGH);
        digitalWrite(PIN_LED_B,HIGH);
      
       if( 60 > hue && hue >= 0){
         digitalWrite(PIN_LED_R,LOW);
       } else if( 180 > hue && hue >= 60 ){
         digitalWrite(PIN_LED_G,LOW);
       }
       else if( 300 > hue && hue >= 180 ){
         digitalWrite(PIN_LED_B,LOW);
       }
       else if( 360 >= hue && hue >= 300){
         digitalWrite(PIN_LED_R,LOW);
       }

      } else {
        digitalWrite(PIN_LED_R, HIGH);
        digitalWrite(PIN_LED_G,HIGH);
        digitalWrite(PIN_LED_B,HIGH);
        
        bigpixel.clear();
        bigpixel.show();
        
        smallpixel.clear();
        smallpixel.show();
      }
  }
  //button is closed
  else {
    if(wasopen){
      state = !state;
      wasopen = false;
    }
    }
}

void Digitdisplay() {
    if(hue == 0){
      tm1637.displayStr("   0");
    }
    else {
    tm1637.displayNum(hue);
    }
    delay(50);
}