#include <Adafruit_NeoPixel.h>

#define NUMPIXELS 1
int power = 11;
int out = 0;
int PIN  = 12;
int powerread = 26;
float hue = 0;
bool state = true;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(power, OUTPUT);
  digitalWrite(power,HIGH);
  pinMode(out,OUTPUT);
  digitalWrite(out, HIGH);
  pinMode(powerread, INPUT_PULLUP);
  pixels.begin();
  pixels.setBrightness(10);
  pixels.clear();

  pinMode(PIN_LED_G,OUTPUT);
  pinMode(PIN_LED_R,OUTPUT);
  pinMode(PIN_LED_B,OUTPUT);
  
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
    Serial.println("Set hue between 0 and 360, hue set to 0");
    hue = 0;
    }
  }

  digitalWrite(PIN_LED_R, HIGH);
  digitalWrite(PIN_LED_G,HIGH);
  digitalWrite(PIN_LED_B,HIGH);
  
  if( 60 > hue && hue >= 0){
    digitalWrite(PIN_LED_R,LOW);
  } else if( 180 > hue && hue >= 60 ){
    digitalWrite(PIN_LED_G,LOW);
  }
  else if( 300 > hue && hue >= 180 ){
    digitalWrite(PIN_LED_B,LOW);
  }
  else if( 360 >= hue && hue >= 300){
    digitalWrite(PIN_LED_R,LOW);
  }

  //button is open
  if(digitalRead(powerread) == HIGH) {
      if(state){
        pixels.clear();
        float to16bit = (hue / 360.0) * 65535.0;
        pixels.setPixelColor(0,pixels.ColorHSV(to16bit,255,255));
        pixels.show();
      } else {
        pixels.clear();
        pixels.show();
      }
  }
  //button is closed
  else {
    state = !state;
      delay(500);
    }
}